#!/bin/bash
ln -sir gitconfig ~/.gitconfig
mkdir -p ~/bin
ln -sir git-* ~/bin

if [ -t 1 ]; then
    set -e
    read -erp "Value to user.name: "
    git config -f ~/.gitconfig.local user.name "$REPLY"
else
    echo "(user.name needs to be set.)"
fi

echo "NOTE:"
echo "NOTE: put your local config in ~/.gitconfig.local"
echo "NOTE:"
